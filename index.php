<?php
/**
 * Plugin.
 * @package reactplug
 * @wordpress-plugin
 * Plugin Name:     Daxko Child Care Plugin
 * Description:     A plugin to manage child care drop off and pick up registration
 * Author:          DAXKO
 * Version:         1.0.0
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

// Include the DXKOChildCareAPI.php file
require_once plugin_dir_path(__FILE__) . 'DXKOChildCareAPI.php';  
// Include the api-endpoint.php file
require_once plugin_dir_path(__FILE__) . 'api-endpoint.php';  

class DXKOClubChildCarePlugin
{
    // Holds the values to be used in the fields callbacks
    private $options;

    // Start up
    public function __construct()
    {   // add settings
        add_action(
            'admin_menu',// hook
            array($this, 'add_admin_menu') // callback function
        );
        // register settings
        add_action(
            'admin_init',// hook
            array($this, 'settings_init') // callback function
        );
        // register shortcode
        add_shortcode(
            'displayChildCarePlugin',// shortcode name
            array($this, 'displayChildCarePlugin') // callback function
        );
        // enqueue react app
        add_action(
            'wp_enqueue_scripts',// hook
            array($this, 'enq_react') // callback function
        );
        add_action( 'admin_enqueue_scripts', // hook
        array($this,'dxkocs_enqueue_admin_script') // callback function
        );
        // add_action( 'rest_api_init', // hook
        // array($this,'register_api_endpoint') // callback function
        // );
    }

    // function register_api_endpoint() {
    //     register_rest_route( 'dxkocc/v1', '/childcare', array(
    //         'methods' => 'POST',
    //         'callback' => array($this, 'dxkocc_custom_endpoint_callback'),
    //     ) );
    // }

    // function dxkocc_custom_endpoint_callback( $request ) {
    //     // Instantiate the class
    //     $dxkocc_api = new DXKOChildCareAPI();

    //     // Extract request parameters
    //     // $action = $request->get_param( 'action' );
    //     $action = 'users/validate-user-credentials';
    //     // $method = $request->get_param( 'method' );
    //     $method = 'post';
    //     // $params = $request->get_param( 'params' );
    //     $params = array(
    //         'username' => 'ckim',
    //         'password' => 'Password1'
    //     );

    //     // Send the request using the class method
    //     $response = $dxkocc_api->dxkocc_send_request( $action, $method, $params );

    //     // Return the response
    //     return rest_ensure_response( $response );
    // }
    
    // initialize settings
    function settings_init()
    {
        // SECTION : USER SETTINGS
        add_settings_section(
            'user_section',// section name
            'User Settings',// display name
            null,// subtitle
            'daxko-child-care-settings' // page to display on
        );

        // ------------------------------
        // SECTION : THEME SETTINGS
        add_settings_section(
            'theme_section',// section name
            'Theme Settings',// display name
            null,// subtitle
            'daxko-child-care-settings' // page to display on
        );

        // ------------------------------
        // SETTINGS OPTION : API KEY
        add_settings_field(
            'API_Key',// setting ID
            'API Key',// display name
            array($this, 'APIKeyHTML'),// callback function
            'daxko-child-care-settings',// page to display on
            'user_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// option group
            'API_Key' // option name
        );

        // ------------------------------
        // SETTINGS OPTION : CLUB NAME
        add_settings_field(
            'Club_Name',// setting ID
            'Club Name',// display name
            array($this, 'ClubNameHTML'),// callback function
            'daxko-child-care-settings',// page to display on
            'user_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// option group
            'Club_Name' // option name
        );

        // ------------------------------
        // SETTINGS OPTION : LOGO
        add_settings_field(
            'Logo_URL',// setting ID
            'Logo URL',// display name
            array($this, 'LogoHTML'),// callback function
            'daxko-child-care-settings',// page to display on
            'theme_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// option group
            'Logo_URL' // option name
        );

        // ------------------------------
        // SETTINGS OPTION : LOGO SMALL
        add_settings_field(
            'Logo_Small_URL',// setting ID
            'Small Logo URL',// display name
            array($this, 'LogoSmallHTML'),// callback function
            'daxko-child-care-settings',// page to display on
            'theme_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// option group
            'Logo_Small_URL' // option name
        );

        // ------------------------------
        // SETTINGS OPTION : PRIMARY COLOR
        add_settings_field(
            'primary_color',// setting ID
            'Primary Color',// display name
            array($this, 'primaryColor'),// callback function
            'daxko-child-care-settings',// page to display on
            'theme_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// Option group
            'dxkocc_theme_colors',// Option name
            array($this, 'sanitize') // Sanitize
        );

        // ------------------------------
        // SETTINGS OPTION : SECONDARY COLOR
        add_settings_field(
            'secondary_color',// setting ID
            'Secondary Color',// display name
            array($this, 'secondaryColor'),// callback function
            'daxko-child-care-settings',// page to display on
            'theme_section' // section name
        );
        register_setting(
            'daxko-child-care-settings',// Option group
            'dxkocc_theme_colors',// Option name
            array($this, 'sanitize') // Sanitize
        );

        // ------------------------------
        // SETTINGS OPTION : TERTIARY COLOR
        add_settings_field(
            'tertiary_color',// setting ID
            'Tertiary Color',// display name
            array($this, 'tertiaryColor'),// callback function
            'daxko-child-care-settings',// page to display on
            'theme_section' // section name
        );
    }

    // ADD ADMIN MENU
    function add_admin_menu()
    {
        add_options_page(
            'Daxko Child Care Plugin Settings',// page title
            'Child Care Plugin',// menu title
            'manage_options',// capability
            'daxko-child-care-settings',// menu slug
            array($this, 'settingsHTML') // callback function
        );
    }

    // ------------------------------
    // CALLBACK : API KEY HTML
    function APIKeyHTML()
    {
        ?>
        <input type="text" name="API_Key" value="<?php echo get_option('API_Key'); ?>" />
        <?php
    }

    // ------------------------------   
    // CALLBACK : CLUB NAME HTML
    function ClubNameHTML()
    {
        ?>
        <input type="text" name="Club_Name" value="<?php echo get_option('Club_Name'); ?>" />
        <?php
    }

    //`------------------------------
    // CALLBACK : LOGO SMALL HTML
    function LogoSmallHTML()
    {
        ?>
        <input type="text" name="Logo_Small_URL" value="<?php echo get_option('Logo_Small_URL'); ?>" />
        <?php
    }

    // ------------------------------
    // CALLBACK : LOGO
    function LogoHTML()
    {
        ?>
        <input type="text" name="Logo_URL" value="<?php echo get_option('Logo_URL'); ?>" />
        <?php
    }
    
    // CALLBACK : PRIMARY COLOR
    function primaryColor()
    {
        $cpickname = 'CCC_primary_color';

        $value = isset( $this->options[ $cpickname ] ) ? $this->options[ $cpickname ] : '#cccccc';

        echo '<input type="text" name="dxkocc_theme_colors[' . $cpickname . ']" value="' . esc_attr($value) . '" id="txtbx__' . $cpickname . '" class="dxkocs-color-fields" data-coloris>
              <span id="reset__' . $cpickname . '" class="admin-small-texts dxkocs-cp-reset dxkocs-hide">Reset</span>
              <input type="hidden" name="dxkocc_theme_colors[' . $cpickname . '_vali]" value="' . esc_attr($value) . '" id="' . $cpickname . '_vali">';
      
    }

    // CALLBACK : SECONDARY COLOR
    function secondaryColor()
    {
        $cpickname = 'CCC_secondary_color';

        $value = isset( $this->options[ $cpickname ] ) ? $this->options[ $cpickname ] : '#cccccc';

        echo '<input type="text" name="dxkocc_theme_colors[' . $cpickname . ']" value="' . esc_attr($value) . '" id="txtbx__' . $cpickname . '" class="dxkocs-color-fields" data-coloris>
              <span id="reset__' . $cpickname . '" class="admin-small-texts dxkocs-cp-reset dxkocs-hide">Reset</span>
              <input type="hidden" name="dxkocc_theme_colors[' . $cpickname . '_vali]" value="' . esc_attr($value) . '" id="' . $cpickname . '_vali">';
      
    }

    // ------------------------------
    // CALLBACK : TERTIARY COLOR
    function tertiaryColor()
    {
        $cpickname = 'CCC_tertiary_color';

        $value = isset( $this->options[ $cpickname ] ) ? $this->options[ $cpickname ] : '#cccccc';

        echo '<input type="text" name="dxkocc_theme_colors[' . $cpickname . ']" value="' . esc_attr($value) . '" id="txtbx__' . $cpickname . '" class="dxkocs-color-fields" data-coloris>
              <span id="reset__' . $cpickname . '" class="admin-small-texts dxkocs-cp-reset dxkocs-hide">Reset</span>
              <input type="hidden" name="dxkocc_theme_colors[' . $cpickname . '_vali]" value="' . esc_attr($value) . '" id="' . $cpickname . '_vali">';
      
    }

    // CALLBACK : SETTINGS HTML
    function settingsHTML()
    {
        $this->options = get_option('dxkocc_theme_colors');
        ?>
        <div class="wrap">
            <h1>Daxko Child Care Plugin Settings</h1>
            <form method="POST" action="options.php">
                <?php
                settings_fields('daxko-child-care-settings');
                do_settings_sections('daxko-child-care-settings');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    //CALLBACK : ENQUEUE ADMIN SCRIPT
    function dxkocs_enqueue_admin_script() {
        // color picker | CSS | https://coloris.js.org/
        wp_enqueue_style( 'dxkocs-custom-coloris', plugin_dir_url(__FILE__) . './coloris/coloris.css' );
        // color picker | JS
        wp_register_script( 'dxkocs-custom-coloris-script', plugin_dir_url(__FILE__) . './coloris/coloris.js' );
        wp_enqueue_script( 'dxkocs-custom-coloris-script' );
    }


    // ------------------------------
    // SHORTCODE : DISPLAY REACT APP
    function displayChildCarePlugin()
    {
        $current_user = (array) wp_get_current_user()->roles;
        ob_start();
        ?>
        <div id="dxko-child-care-plugin"></div>
        <?php return ob_get_clean();
    }

    
    //ENQUEUE REACT APP
    function enq_react()
    {
        wp_register_script(
            'display-react', //handle
            plugin_dir_url(__FILE__) . '/build/index.js',//source
            ['wp-element'],//dependencies
            rand(),// Change this to null for production
            true
        );

        // pass data to react app
        $site_url = get_site_url();
        $current_user = wp_get_current_user();
        $settings = get_option('dxkocc_theme_colors');
        $club_name = get_option('Club_Name');
        $logo_url = get_option('Logo_URL');
        $small_logo_url = get_option('Logo_Small_URL');

      
        $data = array(
            'email' => $current_user->user_email,
            'themeSettings' => $settings,
            'serverUrl' => 'http://localhost:5000',
            'clubName' => $club_name,
            'logoUrl' => $logo_url,
            'smallLogoUrl' => $small_logo_url,
            'siteUrl' => $site_url,
        );
        wp_localize_script('display-react', 'object_cc', $data); //localize script to pass PHP data to JS
        wp_enqueue_script('display-react'); //enqueue script
        wp_enqueue_style('plugin-style', plugin_dir_url(__FILE__) . '/build/style-index.css'); //enqueue style
    }
}

new DXKOClubChildCarePlugin();