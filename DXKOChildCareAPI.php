<?php

// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) exit;

// function debug_to_console($data) {
//     $output = $data;
//     if (is_array($output))
//         $output = implode(',', $output);

//     echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
// }

// $token = get_option('API_key');

// debug_to_console($token);

class DXKOChildCareAPI {

    public function dxkocc_send_request( $action, $method, $params ) {

        // Create an instance so we can use non-static class functions
        $current = new self();

        $request_url = 'https://api.partners.daxko.com/';
        $request_url .= $action; // concatenate URLs

        // Set the request headers
        $data = array(
            // 'timeout' => 45, | 9ca322c5-6d67-4e5e-b081-27d257d922f5

            'headers' => array(
                'Authorization' => 'Token '.get_option('API_key'),
                'Content-Type' => 'application/json'
            )
        );

        if( $method == 'get' ) {

            if( $params ) {
                $request_url .= '?' . http_build_query( $params );
            }
        
            $response = wp_remote_get( $request_url, $data );


        } elseif( $method == 'post' ) {

            if ( $params ) {
                $data[ 'body' ] = json_encode( $params, false );
            }
    
            $response = wp_remote_post( $request_url, $data );

        }/* else {
            // REQUEST METHOD?
        }*/

        // Log errors and return false or return response body as array
        $acceptable_response_codes = array( 200, 201, 400, 409 );
        
        $response_code = wp_remote_retrieve_response_code( $response );
        $response_body = wp_remote_retrieve_body( $response );
        
        if ( is_wp_error( $response ) ) {
            $current->log_error( $action, $response->get_error_message() );
            
            return false;
        } elseif ( ! in_array( $response_code, $acceptable_response_codes ) ) {
            $message = sprintf( 'API responded with code %s.', $response_code );
            $current->log_error( $action, $message );
            
            return false;
        } else {
            return json_decode( $response_body, true );
        }	

    }
    

    public function log_error( $action, $result ) {
        $message = sprintf( 'The Daxko API could not complete the %s request due to the following: %s', 
        $action, 
        $result 
    ); 
    error_log( $message );
    }

}


