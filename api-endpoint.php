<?php

// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) exit;

function dxkocc_custom_endpoint_callback( $request ) {
    // Instantiate the class
    $dxkocc_api = new DXKOChildCareAPI();

    // Extract request parameters
    $action = $request->get_param( 'action' );
    $method = $request->get_param( 'method' );
    $params = $request->get_param( 'params' );

    // Send the request using the class method
    $response = $dxkocc_api->dxkocc_send_request( $action, $method, $params );


    // Return the response
    return rest_ensure_response( $response );
    // return rest_ensure_response( 'yes' );


}

add_action( 'rest_api_init', function () {
    register_rest_route( 'dxkocc/v1', '/childcare', array(
        'methods' => 'post',
        'callback' => 'dxkocc_custom_endpoint_callback',
        'args' => array(),
        // 'permission_callback' => '__return_true' 
    ) );
} );
