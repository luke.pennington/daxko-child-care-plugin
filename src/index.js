const { render } = wp.element;
import App from "./App";
import { store, persistor } from "./redux/store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import "./style.css";

export default App;

if (document.getElementById("dxko-child-care-plugin")) {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </PersistGate>
    </Provider>,
    document.getElementById("dxko-child-care-plugin")
  );
}
