export function getPreviousSection(currentSection) {
  const sectionNumber = parseInt(currentSection.split("-")[1]);
  if (!isNaN(sectionNumber)) {
    const previousSectionNumber = sectionNumber - 1;
    return `section-${previousSectionNumber}`;
  }
  return currentSection;
}
