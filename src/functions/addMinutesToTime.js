export function addMinutesToTime(initialTime, minutesToAdd) {
  const [hourString, minuteString] = initialTime.split(":");
  const [hour, minute] = [parseInt(hourString), parseInt(minuteString)];

  const newMinute = (minute + minutesToAdd) % 60;
  const additionalHour = Math.floor((minute + minutesToAdd) / 60);

  let newHour = hour + additionalHour;
  if (newHour > 12) {
    newHour -= 12;
  }

  const period = hour < 12 ? "am" : "pm";

  return `${newHour.toString().padStart(2, "0")}:${newMinute
    .toString()
    .padStart(2, "0")}${period}`;
}
