import axios from "axios"; // Import Axios

export async function checkInDependents({
  userId,
  estimatedPickup,
  roomId,
  dependentIdArray,
  setCheckedInResponse,
}) {
  // GRAB SITE URL FROM WP object_cc
  const siteUrl = object_cc.siteUrl;

  const endpointURL = `${siteUrl}/wp-json/dxkocc/v1/childcare`; // Update with your site URL

  // to be used in params for dependents
  const dependentsArray = [];
  dependentIdArray.forEach((dependentId) => {
    dependentsArray.push({
      userId: dependentId,
      estimatedPickup: estimatedPickup,
      roomId: roomId,
      overrideAccess: true, //
      guardianLocationId: null, // can be null
      employeeId: null, // can be null
    });
  });

  console.log(dependentsArray);
  const payload = {
    action: `api/v1/childcare/dropoff`,
    method: "post",
    params: {
      dependents: [
        {
          guardianId: userId,
          dependents: dependentsArray,
        },
      ],
    },
  };

  console.log(payload.params);

  try {
    const response = await axios.post(endpointURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.status === 200) {
      const responseData = response.data;
      console.log(responseData);
      setCheckedInResponse(responseData);
    } else {
      console.error("Request failed:", response.data);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}

// DATA TEMPLATE
// const data = {
//   dependents: [
//     {
//       guardianId: 14,
//       dependents: [
//         {
//           userId: 54,
//           estimatedPickup: "07:00:00",
//           roomId: 2,
//           overrideAccess: true, //
//           guardianLocationId: 50, // can be null
//           employeeId: 647, // can be null
//         },
//       ],
//     },
//   ],
// };
