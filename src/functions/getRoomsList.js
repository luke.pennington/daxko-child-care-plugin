import axios from "axios"; // Import Axios

export async function getRoomsList({
  setRoomsList,
  locationId,
  dependentId,
  duration,
}) {
  // GRAB SITE URL FROM WP object_cc
  const siteUrl = object_cc.siteUrl;

  function getFormattedDate() {
    const today = new Date();
    const year = today.getFullYear();
    const month = (today.getMonth() + 1).toString().padStart(2, "0");
    const day = today.getDate().toString().padStart(2, "0");

    return `${year}-${month}-${day}`;
  }
  const endpointURL = `${siteUrl}/wp-json/dxkocc/v1/childcare`; // Update with your site URL

  const payload = {
    action: `api/v1/childcare/room-list`,
    method: "get",
    params: {
      childId: dependentId,
      date: getFormattedDate(),
      durationMinutes: duration,
      entityId: locationId,
    },
  };

  try {
    const response = await axios.post(endpointURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.status === 200) {
      const responseData = response.data.data;
      setRoomsList(responseData.rooms);
    } else {
      console.error("Request failed:", response.data);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}
