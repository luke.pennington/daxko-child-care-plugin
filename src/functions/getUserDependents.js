import axios from "axios"; // Import Axios

export async function getUserDependents(userId, setEligibleDependents) {
  // GRAB SITE URL FROM WP object_cc
  const siteUrl = object_cc.siteUrl;

  const endpointURL = `${siteUrl}/wp-json/dxkocc/v1/childcare`; // Update with your site URL

  const payload = {
    action: `api/v1/childcare/members/${userId}/eligible-child-list`,
    method: "get",
    params: null,
  };

  try {
    const response = await axios.post(endpointURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.status === 200) {
      const responseData = response.data.data;
      setEligibleDependents(responseData);
    } else {
      console.error("Request failed:", response.data);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}
