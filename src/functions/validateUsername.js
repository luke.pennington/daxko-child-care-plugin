export function validateLogin(formData) {
  const { username, password } = formData;
  console.log("we madae it");
  // Check if the username is at least 4 characters and does not contain spaces
  if (
    username.length >= 4 &&
    !/\s/.test(username) &&
    !/\s/.test(password) &&
    password.length >= 4
  ) {
    return "valid";
  } else if (/\s/.test(username) || /\s/.test(password)) {
    return "Username and password should not contain spaces.";
  } else if (username.length < 4 || password.length < 4) {
    return "Username and password should be at least 4 characters long.";
  }
}
