import axios from "axios"; // Import Axios

export async function userLogin(
  formData,
  dispatch,
  updateCurrentPage,
  updateCurrentUserInfo
) {
  // GRAB SITE URL FROM WP object_cc
  const siteUrl = object_cc.siteUrl;
  const endpointURL = `${siteUrl}/wp-json/dxkocc/v1/childcare`; // Update with your site URL

  const payload = {
    action: "api/v1/users/validate-user-credentials",
    method: "post",
    params: {
      username: `${formData.username}`,
      password: `${formData.password}`,
    },
  };

  try {
    const response = await axios.post(endpointURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.data != "Incorrect Username or Password") {
      const responseData = response.data;
      console.log(response, "response data");
      dispatch(updateCurrentUserInfo({ data: responseData.data })); // add userdata to the store
      dispatch(
        updateCurrentPage({
          loginStatus: true,
          path: "home",
        })
      );
    } else {
      console.error("Request failed:", response.data);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}
