When using file for development:

1. Clone or download repo
2. Add plugin folder to 'your-site-folder' > app > public > wp-content > plugins
3. Add the plugin to your page via short code [displayChildCarePlugin]
4. Open the terminal in the file directory and run "npm i" to install all node dependencies.
5. Use "npm run start" to create the necessary files needed for the plugin to work during development.

When using file for production:

1. When plugin is finalized and ready for deployment open the terminal in the file directory and run "npm run build".
2. When build is done running, delete the "node_modules" folder.
3. Add plugin to WordPress.
4. Add the plugin to your page via short code [displayChildCarePlugin]

FILE(S) descriptions
index.php :
Set up the admin page settings for user to add their API TOKEN and customize theme settings.
api-endpoint.php :
handles the HTTP requests made by the client side and makes a POST request to the DXKOChildCareAPI.php
DXKOChildCareAPI.php : creates the endpoint for the 'https://api.partners.daxko.com/' and handles all requests to the API
src (folder): contains all client side REACT.JS code
src > lib > Login.js : handles user login UI.
src > lib > Home.js : handles main functionality for checking in and out user depents
src > lib > components (folder) : contains resuable components
src > lib > redux : contains all files required to set up redux store
src > functions (folder) : contains all the functions that make the HTTP requests to the api-endpoint.php as well as other helper functions for displaying times, switching between sections ...etc.
