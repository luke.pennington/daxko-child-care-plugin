import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { updateCurrentPage } from "../redux/reducers/currentPageSlice";
import { updateCurrentUserInfo } from "../redux/reducers/currentUserInfo";
import { userLogin } from "../functions/userLogin";
import { validateLogin } from "../functions/validateUsername";
import Alert from "./components/Alert";
const Login = ({ isCurrentPage }) => {
  const [formData, setFormData] = useState({
    username: "ckim",
    password: "Password1",
  });

  const [loginError, setLoginError] = useState({
    message: "",
    error: false,
  });
  const dispatch = useDispatch();
  //get theme settings
  const themeColor1 = object_cc.themeSettings.CCC_primary_color;
  const themeColor2 = object_cc.themeSettings.CCC_secondary_color;
  const themeColor3 = object_cc.themeSettings.CCC_tertiary_color;
  const siteUrl = object_cc.siteUrl;
  const logoUrl = object_cc.logoUrl;
  const logo = object_cc.themeSettings;

  // handle submit form
  const submitForm = (e) => {
    e.preventDefault();
    // check if username and password are not empty
    if (validateLogin(formData) === "valid") {
      // call userLogin function and pass all required parameters
      userLogin(
        formData,
        dispatch,
        updateCurrentPage,
        updateCurrentUserInfo,
        setLoginError
      );
    } else {
      setLoginError({
        message: `${validateLogin(formData)}`,
        error: true,
      });
      setTimeout(() => {
        setLoginError({
          message: "",
          error: false,
        });
      }, 5000); // 5000 milliseconds = 5 seconds
    }
  };

  return (
    <div className={`page-container login-container ${isCurrentPage}`}>
      {/* HEADER */}
      <div
        className="login-header"
        style={{
          backgroundColor: `${themeColor1}`,
          color: "white",
        }}
      >
        <img src={logoUrl} alt="club logo" style={{ marginTop: "10px" }} />
      </div>

      {/* LOGIN FORM */}
      <form className="login-form">
        <div className="login-form-header">
          <h1>Check-Ins</h1>
          <p>Please sign in to continue</p>
        </div>

        <div>
          <label>Username</label>
          <input
            type="text"
            name="username"
            value={formData.username}
            onChange={(e) =>
              setFormData({ ...formData, username: e.target.value })
            }
          />
        </div>
        <div>
          <label>Password</label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={(e) =>
              setFormData({ ...formData, password: e.target.value })
            }
          />
        </div>
        <div className="login-options">
          <div className="remember-me">
            <input type="checkbox" />
            <label>Remember Me</label>
          </div>
          <div className="forgot-password">
            <a href="https://www.w3schools.com">Forgot Password</a>
          </div>
        </div>

        <button
          onClick={submitForm}
          style={{ backgroundColor: `${themeColor1}` }}
        >
          Login
        </button>
      </form>
      {loginError.error ? <Alert message={loginError.message} /> : null}
    </div>
  );
};

export default Login;
