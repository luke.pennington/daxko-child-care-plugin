import React from "react";

//HOOKS
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// COMPONENTS
import SignOutButton from "./components/signOutButton";
import CheckInOptions from "./components/CheckInOptions";
import ChildCheckCard from "./components/ChildCheckCard";
import ContinueButton from "./components/ContinueButton";
import BackButton from "./components/backButton";

// Helper Functions
import { getUserDependents } from "../functions/getUserDependents";
import { getLocationsData } from "../functions/getLocationsData";
import { getRoomsList } from "../functions/getRoomsList";
import { addMinutesToTime } from "../functions/addMinutesToTime";
import { getPreviousSection } from "../functions/getPreviousSection";
import { checkInDependents } from "../functions/checkInDependents";
// HOME PAGE
const Home = ({ isCurrentPage }) => {
  // SETTINGS FROM WORDPRESS
  const themeColor1 = object_cc.themeSettings.CCC_primary_color;
  const clubName = object_cc.clubName;
  const smallLogoUrl = object_cc.smallLogoUrl;

  // USESTATE DATA
  // list of eligible dependents
  const [eligibleDependents, setEligibleDependents] = useState([{}]);
  // determine if user is checking in or out
  const [checkIn, setCheckIn] = useState(true);
  // location options
  const [locationsData, setLocationsData] = useState([]);
  // duration options
  const [durationTime, setDurationTime] = useState(30);
  // get avialablerooms list
  const [roomsList, setRoomsList] = useState([]);
  // set room selection
  const [selectedRoom, setSelectedRoom] = useState({
    id: roomsList[0]?.id,
    name: roomsList[0]?.name,
    timeSlots: roomsList[0]?.timeSlots,
  });
  // set selected time slot
  const [selectedTimeSlot, setSelectedTimeSlot] = useState(
    roomsList[0]?.timeSlots[0]
  );
  // set selected child
  const [selectedChildren, setSelectedChildren] = useState([]);
  // set current section
  const [currentSection, setCurrentSection] = useState("section-1");
  // set current location
  const [currentLocation, setCurrentLocation] = useState({
    name: locationsData[0]?.title,
    id: locationsData[0]?.id,
  });
  // set checked in response
  const [checkedInResponse, setCheckedInResponse] = useState("");

  // GET DATA FROM REDUX STORE
  // get user id
  const userId = useSelector((state) => state.currentUser.data.userId);
  // get current page
  const currentPage = useSelector((state) => state.currentPage.path);
  // get user first name
  const userFirstName = useSelector(
    (state) => state.currentUser.data.firstName
  );

  // FUNCTIONALITY
  // Only fetch eligible dependents if the current page is the home page
  useEffect(() => {
    if (isCurrentPage === "current-page") {
      // get eligible dependents function from getUserDependents.js
      getUserDependents(userId, setEligibleDependents);
      // get locations data function from getLocationsData.js
      getLocationsData(setLocationsData);
      // set current section to section-1 when page loads
      setCurrentSection("section-1");
    }
  }, [currentPage]);

  // Update currentLocation selection when locationsData changes
  useEffect(() => {
    if (isCurrentPage === "current-page") {
      setCurrentLocation({
        name: locationsData[0]?.title,
        id: locationsData[0]?.id,
      });
    }
  }, [locationsData]);

  // Fetch available rooms and set roomsList when duration changes
  useEffect(() => {
    if (currentSection == "section-2") {
      getRoomsList({
        setRoomsList: setRoomsList,
        locationId: currentLocation.id,
        dependentId: selectedChildren[0]?.id,
        duration: durationTime,
      });
    }
  }, [currentSection, currentLocation]);

  useEffect(() => {
    if (currentSection == "section-2") {
      setSelectedRoom({
        id: roomsList[0].id,
        name: roomsList[0].name,
        timeSlots: roomsList[0].timeSlots,
      });
    }
  }, [roomsList]);

  // update time slots if user changes room selection
  useEffect(() => {
    if (currentSection == "section-2") {
      setSelectedTimeSlot(selectedRoom?.timeSlots[0]);
    }
  }, [selectedRoom]);

  // For checking if section is the current section. If it is, add class name 'current-section'
  // If it is not, add class name 'not-current-section'
  const isCurrentSection = (section) => {
    if (currentSection === section) {
      return "current-section";
    } else {
      return "not-current-section";
    }
  };

  // Drop off dependents
  const dropOffDependents = () => {
    checkInDependents({
      userId: userId,
      estimatedPickup: addMinutesToTime(selectedTimeSlot, durationTime),
      roomId: selectedRoom.id,
      dependentIdArray: selectedChildren.map((child) => child.id),
      setCheckedInResponse: setCheckedInResponse,
    });
  };

  return (
    <div className={`page-container home-container ${isCurrentPage}`}>
      <div className="home-content">
        {/* HEADER */}
        <header
          className="home-header-container padding20"
          style={{ backgroundColor: `${themeColor1}`, color: "white" }}
        >
          <div className="greeting-logo-container">
            <img src={smallLogoUrl} style={{ width: "40px" }} />
            <div className="greeting">
              <h2>Hey {userFirstName},</h2>
              <p>Welcome to {clubName}</p>
            </div>
          </div>

          <SignOutButton />
        </header>

        {/* SECTION 1 - User chooses if they are CHECK IN or
         CHECK OUT dependent as well as selecting which 
         dependents to check out */}
        <div className={`section-1 ${isCurrentSection("section-1")}`}>
          <CheckInOptions setCheckIn={setCheckIn} checkIn={checkIn} />

          <div className="child-check-cards-container">
            <p>Please select dependents to check in</p>
            {eligibleDependents[0].id
              ? eligibleDependents.map((child) => (
                  <ChildCheckCard
                    key={child.id}
                    child={child}
                    selectedChildren={selectedChildren}
                    setSelectedChildren={setSelectedChildren}
                  />
                ))
              : null}
          </div>

          <div className="continue-button-container">
            <ContinueButton
              showButton={selectedChildren.length >= 1 ? true : false}
              color={themeColor1}
              text="CONTINUE"
              setCurrentSection={setCurrentSection}
              nextSection={checkIn ? "section-2" : "section-3"}
            />
          </div>
        </div>

        {/*  SECTION 2 - Only available if user choses CHECK IN,
        user can then select which room,   */}
        <div
          className={`section-2 padding20  ${isCurrentSection("section-2")}`}
        >
          <form>
            <div className="option-selector location-selector">
              <label>Select your location</label>
              <select
                value={currentLocation.id}
                onChange={(e) =>
                  setCurrentLocation({
                    id: e.target.value,
                    name: e.target.selectedOptions[0].text,
                  })
                }
              >
                {locationsData.map((location) => (
                  <option value={location.id} key={location.id}>
                    {location.title}
                  </option>
                ))}
              </select>
            </div>

            <div className="option-selector room-selector">
              <label>Select Room</label>
              <select
                name="select room"
                value={selectedRoom?.roomId} // Assuming selectedRoom is an object with roomId property
                onChange={(e) => {
                  const selectedValue = e.target.value;
                  setSelectedRoom(
                    roomsList.find((room) => room.id == selectedValue)
                  );
                }}
              >
                {roomsList ? (
                  roomsList.map((room) => (
                    <option value={room.id} key={room.id}>
                      {room.name}
                    </option>
                  ))
                ) : (
                  <option value={0}>No rooms available</option>
                )}
              </select>
            </div>

            <div className="option-selector time-slot-selector">
              <label>Select Time Slot</label>
              <select
                name="time slot"
                value={selectedTimeSlot} // Assuming selectedRoom is an object with timeSlots property
                onChange={(e) => setSelectedTimeSlot(e.target.value)}
              >
                {selectedRoom?.timeSlots?.length > 0 ? (
                  selectedRoom?.timeSlots?.map((slot) => (
                    <option value={slot} key={slot}>
                      {slot}
                    </option>
                  ))
                ) : (
                  <option>not times available</option>
                )}
              </select>
            </div>

            <div className="option-selector duration-selector">
              <label>Select Duration</label>
              <select
                name="duration"
                value={durationTime}
                onChange={(e) => setDurationTime(e.target.value)}
              >
                <option value={15}>15 minutes</option>
                <option value={30}>30 minutes</option>
                <option value={45}>45 minutes</option>
                <option value={60}>60 minutes</option>
              </select>
            </div>
          </form>
          <div className="continue-button-container">
            <ContinueButton
              showButton={selectedTimeSlot ? true : false}
              color={themeColor1}
              text="CONTINUE"
              setCurrentSection={setCurrentSection}
              nextSection={"section-3"}
            />
          </div>
        </div>

        {/* SECTION 3 - Summary of check in */}
        <div
          className={`section-3 padding20  ${isCurrentSection("section-3")}`}
        >
          <h2>Summary</h2>

          <div className="summary dependents">
            <h4>Dependents:</h4>
            <div>
              {selectedChildren.map((child) => (
                <p>{child.fullname}</p>
              ))}
            </div>
          </div>

          <div className="summary location">
            <h4>Location:</h4>
            <p>{currentLocation.name}</p>
          </div>

          <div className="summary room">
            <h4>Room:</h4>
            <p>{selectedRoom?.name}</p>
          </div>

          <div className="summary time-slot">
            <h4>Time Slot:</h4>
            <div className="time-summaries">
              <p>
                {`${selectedTimeSlot} - ${
                  selectedTimeSlot
                    ? addMinutesToTime(selectedTimeSlot, durationTime)
                    : ""
                } `}
              </p>
            </div>
          </div>

          <ContinueButton
            showButton={true}
            color={themeColor1}
            text="CHECK IN"
            setCurrentSection={setCurrentSection}
            nextSection={"section-3"}
            action={dropOffDependents}
          />
        </div>

        {/* FOOTER */}
        <div
          className="home-footer"
          style={{
            backgroundColor: `${themeColor1}`,
          }}
        >
          <BackButton
            setCurrentSection={setCurrentSection}
            section={getPreviousSection(currentSection)}
            showButton={currentSection == "section-1" ? false : true}
          />
        </div>
      </div>
    </div>
  );
};

export default Home;
