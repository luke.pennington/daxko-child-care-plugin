import React from "react";

const CheckInOptions = ({ setCheckIn, checkIn }) => {
  return (
    <div className="check-in-options-container">
      <div className="check-in-option">
        <div className="buttons-container">
          <button
            className={`check-in-button ${checkIn ? "active" : ""}`}
            onClick={() => setCheckIn(true)}
          >
            Check In
          </button>
          <button
            className={`check-out-button ${checkIn ? "" : "active"}`}
            onClick={() => setCheckIn(false)}
          >
            Check Out
          </button>
        </div>
      </div>
    </div>
  );
};

export default CheckInOptions;
