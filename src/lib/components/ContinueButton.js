import React from "react";

const ContinueButton = ({
  showButton,
  color,
  text,
  setCurrentSection,
  nextSection,
  action,
}) => {
  return (
    <>
      {showButton ? (
        <button
          style={{ backgroundColor: `${color}` }}
          onClick={() => {
            setCurrentSection(nextSection);
            action ? action() : null;
          }}
        >
          {text}
        </button>
      ) : null}
    </>
  );
};

export default ContinueButton;
