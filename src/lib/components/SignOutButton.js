import React from "react";
import { updateCurrentPage } from "../../redux/reducers/currentPageSlice";
import { updateCurrentUserInfo } from "../../redux/reducers/currentUserInfo";
import { useDispatch, useSelector } from "react-redux";
import logOut from "../../images/logout.png";
const SignOutButton = () => {
  const dispatch = useDispatch();
  // sign out function
  const signOut = (e) => {
    e.preventDefault();
    // update current page url / edit for once api access is granted
    dispatch(
      updateCurrentPage({
        loginStatus: false,
        path: "login",
      })
    );
    // clear user data from store

    setTimeout(() => {
      dispatch(
        updateCurrentUserInfo({
          data: {},
        })
      );
    }, 2000); // 5000 milliseconds = 5 seconds
  };

  return (
    <button onClick={signOut} className="logout-button" id="logout-button">
      <img src={logOut} className="logout-icon" alt="log out icon" />
    </button>
  );
};

export default SignOutButton;
