import React from "react";
import alert from "../../images/alert.png";

const Alert = ({ message }) => {
  return (
    <div className="dxko-plugin-alert">
      <img src={alert} alt="alert icon" className="alert-icon" />
      <p>{message}</p>
    </div>
  );
};

export default Alert;
