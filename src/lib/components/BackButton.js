import React from "react";

import backButton from "../../images/back-button.png";
const BackButton = ({ setCurrentSection, section, showButton }) => {
  return (
    <div>
      {showButton ? (
        <button
          className="back-button"
          onClick={() => setCurrentSection(section)}
        >
          <img src={backButton} alt="back button" style={{ height: "16px" }} />
        </button>
      ) : null}
    </div>
  );
};

export default BackButton;
