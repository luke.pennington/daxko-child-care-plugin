import React from "react";
import { useEffect, useState } from "react";
import checkMark from "../../images/check_mark.png";
const ChildCheckCard = ({
  key,
  child,
  selectedChildren,
  setSelectedChildren,
}) => {
  const [selected, setSelected] = useState(false);
  // get theme color from admin settings
  const themeColor1 = object_cc.themeSettings.CCC_primary_color;

  const toggleChild = (id, fullname) => {
    const existingChildIndex = selectedChildren.findIndex(
      (child) => child.id === id
    );

    if (existingChildIndex !== -1) {
      // Child exists, remove it
      const updatedChildList = [...selectedChildren];
      updatedChildList.splice(existingChildIndex, 1);
      setSelectedChildren(updatedChildList);
      setSelected(false);
    } else {
      // Child does not exist, add it
      const newChild = { id, fullname };
      setSelectedChildren([...selectedChildren, newChild]);
      setSelected(true);
    }
  };

  return (
    <div className="child-check-card" id={`${key}`}>
      <button
        className="check-box"
        style={{ backgroundColor: `white` }}
        onClick={() => toggleChild(child.id, child.fullName)}
      >
        {selected ? <img src={checkMark} alt="" /> : <></>}
      </button>
      <h4>{child.fullName}</h4>
    </div>
  );
};

export default ChildCheckCard;
