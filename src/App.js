import React, { useState, useEffect } from "react";
import Login from "./lib/Login";
import Home from "./lib/Home";
import { useSelector, useDispatch } from "react-redux";

const App = () => {
  let pageStatus = useSelector((state) => state.currentPage.path);
  let userStatus = useSelector((state) => state.currentPage.loginStatus);

  //returns true or false if the page is the current page
  function isCurrentPage(page) {
    // if user is logged in and page matches current page
    if (pageStatus === page && userStatus === true) {
      return "current-page";
    } else {
      return "not-current-page";
    }
  }

  return (
    <div className="app-container">
      {/* if user is signed in: don't display. If user is NOT signed in: display */}
      <Login isCurrentPage={userStatus ? "not-current-page" : "current-page"} />
      <Home isCurrentPage={isCurrentPage("home")} />
    </div>
  );
};
export default App;
