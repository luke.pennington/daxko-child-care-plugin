import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // You can change the storage type as needed
import currentPageReducer from "./reducers/currentPageSlice";
import currentUserInfoReducer from "./reducers/currentUserInfo";
import thunk from "redux-thunk";

// Configuration for Redux Persist
const persistConfig = {
  key: "root", // Storage key
  storage, // Storage type (local storage in this case)
};

const persistedReducer = persistReducer(
  persistConfig,
  combineReducers({
    currentPage: currentPageReducer,
    currentUser: currentUserInfoReducer,
  })
);

export const store = configureStore({
  reducer: persistedReducer, // Use the persisted reducer
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
});

export const persistor = persistStore(store); // Create a persistor object

// You can export both the store and persistor
