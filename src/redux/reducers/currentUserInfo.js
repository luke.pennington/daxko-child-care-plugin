import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {},
};

export const currentUserInfoSlice = createSlice({
  name: "Current Page",
  initialState,
  reducers: {
    updateCurrentUserInfo: (state, action) => {
      state.data = action.payload.data;
    },
  },
});

export const { updateCurrentUserInfo } = currentUserInfoSlice.actions;
export default currentUserInfoSlice.reducer;
