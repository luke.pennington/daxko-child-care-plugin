import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loginStatus: false, // reverse later
  path: "login", // set to login later
};

export const currentPageSlice = createSlice({
  name: "Current Page",
  initialState,
  reducers: {
    updateCurrentPage: (state, action) => {
      state.loginStatus = action.payload.loginStatus;
      state.path = action.payload.path;
    },
  },
});

export const { updateCurrentPage } = currentPageSlice.actions;
export default currentPageSlice.reducer;
